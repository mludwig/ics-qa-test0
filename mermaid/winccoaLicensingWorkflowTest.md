# SAS Support Procedure: WinCC-OA Licensing Workflow
- mermaid example
- reproducing https://confluence.cern.ch/display/BEICSS/WinCC+OA+licensing+workflow
- flow lines cross but are automatic
- simple syntax, integrated with mkdocs, pipeline, gitlab
- mermaid@gitlab (10.7.0) URL links work only with right click, tooltips don't work, underline links does not work 
- The main licensing workflow for WinCC OA 3.19 can be seen below.
- For the complete procedures and more information, please check https://confluence.cern.ch/winccoa/licensing

```mermaid
---
title: "WinCC-OA Licensing Workflow"
config:
   look: classic
   theme: base
   themeVariables: {
   	primaryColor: "#DD3030",
   	primaryTextColor: "#FFF",
   	lineColor: "#F8B229",
   	secondaryColor: "#006100",
   	tertiaryColor: "#FFF",
   	primaryBorderColor: "#7C0000",
   	fontSize: 16,
   	fontFamily: system-ui,
   	fontStyle: italic
   }
   securityLevel: loose
---
flowchart LR

	A(["Start"])
	A1{"Is the user \n registered in the \n WinCC-OA User's DB"}
	
        B1{"Is the Users's \n institute registered in \n the WinCC-OA User's DB?"}
	B2(("Tell the user \n to contact their\ntechnical contact to\n request users's registration"))
	B3(("Ask user to follow \n the institute registration \n procedure"))

	C{"WinCC-OA Version"}
	C2["Escalatate to 3rd line"]	

        E1["Escalate to SAS"]
	E2["won't fix"]
	
	D{"Production \n license"}
	D1{"used by single user \n in CERN network"}
        D2{"ATS (CCR machines)"}
    	D3("Suggest the \nlicense tunnel")
    	    	
    	F["Get license \nrelated information"]
    
    
	A --> A1
	A1 -- | Yes |--> C
	A1 -- | No | --> B1
	B1 -- | No | --> B3
	B1 -- | Yes | --> B2
	B2 --> E2
	B3 --> E2
	C -- | 3.16 | -->C2
	C -- | 3.15 | --> E2
	C -- | 3.19 | --> D
	D -- | No | -->D1
	D -- | Yes | --> D2
	D1 -- | Yes | --> D3
	D1 -- | No | --> D2
	D2 -- | Yes | --> E1	
	D2 -- | No | --> F
	F --> C2
	
click B "https://confluence.cern.ch/display/BEICSS" "OSRM confluence"
click B21 "https://gitlab.cern.ch/industrial-controls/services/qa/qatoolbox/venus-caen/-/blob/master/DEMO.md"

```


