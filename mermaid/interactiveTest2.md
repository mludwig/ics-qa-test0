```mermaid
---
title: VENUS support tree
config:
   look: classic
   theme: base
   themeVariables: {
   	primaryColor: "#DDAA99",
   	primaryTextColor: black,
   	lineColor: blue
   }
   securityLevel: loose
---
flowchart LR

	A(["A configuring"])
	A1["A1 electronic tree definition"]
	A2["A2 (remote) openstack VM config"]
	A3["A3 (local) podman config"]
	A4["A4 versions"]
	
	B(["B runtime"])
	B1["B1 remote (openstack VM)"]
	B2["B2 local (podman container)"]
	B21["B21 local mount /vm-mount not found"]
	B3["B3 electronic tree injection"]
	B4["B4 pilot"]
	B5["B5 caen opc"]
		
	C(["C features"])
	C1["C1 crate related"]
	C2["C2 board related"]
	C3["C3 channel related"]
	C4["C4 behavior related"]
	C5["C5 general"]
	C51["C51 new feature request"]
	C52["C52 documentation"]
	
	D(["something else"])
		
    A --> A1
    A --> A2
    A --> A3
    A --> A4
    
    B --> B1
    B --> B2
    B --> B3
    B --> B4
    B --> B5
    B2 --> B21
    
    C --> C1
    C --> C2
    C --> C3
    C --> C4
    C --> C5
    C5 --> C51
    C5 --> C52
    
click B "https://confluence.cern.ch/display/BEICSS" "OSRM confluence"
click B21 "https://gitlab.cern.ch/industrial-controls/services/qa/qatoolbox/venus-caen/-/blob/master/DEMO.md"

```


